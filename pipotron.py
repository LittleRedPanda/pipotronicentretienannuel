﻿import random 

class Pipotron:
    def __init__(self,prenom="Bob",boss="Marley"):

        self._voyelles="aeiouyéèàîïùê"
        
        self._mot1=[]
        self._mot2=[]
        self._mot3=[]
        self._mot4=[]
        self._mot5=[]
        self._mot6=[]
        self._mot7=[]
        self._mot8=[]

        self._mot1.append("Avec")
        self._mot1.append("Considérant")
        self._mot1.append("Où que nous mène")
        self._mot1.append("Eu égard à")
        self._mot1.append("Vu")
        self._mot1.append("En ce qui concerne")
        self._mot1.append("Dans le cas particulier de")
        self._mot1.append("Quelle que soit")
        self._mot1.append("Du fait de")
        self._mot1.append("Tant que durera")

        self._mot2.append("la situation")
        self._mot2.append("la conjoncture")
        self._mot2.append("la crise")
        self._mot2.append("l'inertie")
        self._mot2.append("l'impasse")
        self._mot2.append("l'extrémité")
        self._mot2.append("la dégradation des moeurs")
        self._mot2.append("la sinistrose")
        self._mot2.append("la dualité de la situation")
        self._mot2.append("la baisse de confiance")

        self._mot3.append("présente")
        self._mot3.append("actuelle")
        self._mot3.append("qui nous occupe")
        self._mot3.append("qui est la nôtre")
        self._mot3.append("induite")
        self._mot3.append("conjoncturelle")
        self._mot3.append("contemporaine")
        self._mot3.append("de cette fin de siècle")
        self._mot3.append("de la société")
        self._mot3.append("de ces derniers temps")
               
        self._mot4.append("il convient d#")
        self._mot4.append("il faut")
        self._mot4.append("on se doit d#")
        self._mot4.append("il est préférable d#")
        self._mot4.append("il serait intéressant d#")
        self._mot4.append("il ne faut pas négliger d#")
        self._mot4.append("on ne peut se passer d#")
        self._mot4.append("il est nécessaire d#")
        self._mot4.append("il serait bon d#")
        self._mot4.append("il faut de toute urgence")      

        self._mot5.append("étudier")
        self._mot5.append("examiner")
        self._mot5.append("ne pas négliger")
        self._mot5.append("prendre en considération")
        self._mot5.append("anticiper")
        self._mot5.append("imaginer")
        self._mot5.append("se préoccuper de")
        self._mot5.append("s'intéresser à")
        self._mot5.append("avoir à l'esprit")
        self._mot5.append("se remémorer")

        self._mot6.append("toutes les")
        self._mot6.append("chacune des")
        self._mot6.append("la majorité des")
        self._mot6.append("toutes les")
        self._mot6.append("l'ensemble des")
        self._mot6.append("la somme des")
        self._mot6.append("la totalité des")
        self._mot6.append("la globalité des")
        self._mot6.append("toutes les")
        self._mot6.append("certaines")
            
        self._mot7.append("issues")
        self._mot7.append("problématiques")
        self._mot7.append("voies")
        self._mot7.append("alternatives")
        self._mot7.append("solutions")
        self._mot7.append("issues")
        self._mot7.append("problématiques")
        self._mot7.append("voies")
        self._mot7.append("alternatives")

        self._mot8.append("envisageables")
        self._mot8.append("possibles")
        self._mot8.append("déjà en notre possession")
        self._mot8.append("s'offrant à nous")
        self._mot8.append("de bon sens")
        self._mot8.append("envisageables")
        self._mot8.append("possibles")
        self._mot8.append("déjà en notre possession")
        self._mot8.append("s'offrant à nous")
        self._mot8.append("de bon sens")
        
        self._mots=[]
        self._mots.append(self._mot1)
        self._mots.append(self._mot2)
        self._mots.append(self._mot3)
        self._mots.append(self._mot4)
        self._mots.append(self._mot5)
        self._mots.append(self._mot6)
        self._mots.append(self._mot7)
        self._mots.append(self._mot8)


        self._alternative3_1=[]
        self._alternative3_2=[]
        
  
        self._alternative3_1.append(boss)
   
        self._alternative3_1.append(prenom)

        self._alternative3_2.append("doit")
        self._alternative3_2.append("devra")
        self._alternative3_2.append("pourrait")
        self._alternative3_2.append("s'appuiera à")

        self._alt=[]
        self._alt.append(self._alternative3_1)
        self._alt.append(self._alternative3_2)

    def generate(self):
        chosenWords=[]
        
        for listeMots in self._mots:
            chosenWords.append(random.choice(listeMots))
        
        #remplacer le # par ' ou e dans mot 4
        mot4=chosenWords[3]
        mot5=chosenWords[4]
  

    
        if mot4[-1]=='#':
            if mot5[0] in self._voyelles:
                mot4=mot4.replace('#',"'")
            else:
                mot4=mot4.replace('#',"e")
  
        chosenWords[3]=mot4

        #alternative mot 3
        i=random.choice([0,1])
        if i==1:
            alt=[]
            for listeMots in self._alt:
                alt.append(random.choice(listeMots))
            chosenWords[3]=' '.join(alt)

        s=' '.join(chosenWords)
        return s
        
            
            
            
        
    
if __name__=='__main__':
    pip=Pipotron()
    print(pip.generate())
    print(pip.generate())
    print(pip.generate())
    print(pip.generate())
    print(pip.generate())
    print(pip.generate())
