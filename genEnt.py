﻿import zipfile
import tempfile
import xml.etree.ElementTree as ET
from pipotron import *
import os
import shutil

#on se base sur le template avec les #observaiton pour trouver ou inserer des trucs a dire

def readDocxAsXml(docxPath):
   document = zipfile.ZipFile(docxPath)
   xmlContent = document.read('word/document.xml')
   document.close()
   return xmlContent

def writeAndCloseDocx (xml_content, output_filename,docxPath):
        """ Create a temp directory, expand the original docx zip.
            Write the modified xml to word/document.xml
            Zip it up as the new docx
        """

        tmp_dir = tempfile.mkdtemp()
        z=zipfile.ZipFile(docxPath)
        z.extractall(tmp_dir)

        with open(os.path.join(tmp_dir,'word/document.xml'), 'w') as f:
            f.write(xml_content)

        # Get a list of all the files in the original docx zipfile
        filenames = z.namelist()
        # Now, create the new zip file and add all the filex into the archive
        zip_copy_filename = output_filename
        with zipfile.ZipFile(zip_copy_filename, "w") as docx:
            for filename in filenames:
                docx.write(os.path.join(tmp_dir,filename), filename)

        # Clean up the temp dir
        shutil.rmtree(tmp_dir)      

if __name__=="__main__":
    prenom=""
    boss=""


    
    ns = {'ns0': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}
    root=ET.fromstring((readDocxAsXml("templateEntAnnuel.docx")))
   
    #print(ET.tostring(root))
    body=root.findall("ns0:body",ns)[0]


    ts=body.findall('.//ns0:t',ns) #tousles t sous body du doc, xpath syntax
    texts=[]
    nextprenom=False
    nextboss=False

    #get prenom et boss
    for t in ts:
       if(nextprenom and prenom==""):
          if(t.text.strip()!=""):
              prenom=t.text
              nextPrenom=False
       elif(nextboss and boss==""):
          if(t.text.strip()!=""):
              boss=t.text
              nextboss=False
       if(prenom=="" and t.text.strip(":").strip()=="Prénom"):
          nextprenom=True
       if(boss=="" and t.text.strip(":").strip()=="Rattachement hiérarchique"):
          nextboss=True

       if(prenom!="" and boss!=""):
           break
    pipotron=Pipotron(prenom,boss)
    #remplir. Appréciations et souhaits du salarié
    for t in ts:
       if "#OBSERVATION" in t.text:
          t.text=pipotron.generate()

    writeAndCloseDocx(str(ET.tostring(root))[2:-1].replace("\\",""),"out.docx","templateEntAnnuel.docx")
